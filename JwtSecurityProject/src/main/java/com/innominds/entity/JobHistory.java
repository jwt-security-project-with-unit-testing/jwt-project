package com.innominds.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "jobHistory")
public class JobHistory {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column
	private int id;
	@Column
	private String jobName;
	@Column
	private String jobDetails;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "userId")
	private User user;



	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(String jobDetails) {
		this.jobDetails = jobDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public JobHistory(int id, String jobName, String jobDetails) {
		super();
		this.id = id;
		this.jobName = jobName;
		this.jobDetails = jobDetails;
	}
	

	public JobHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
