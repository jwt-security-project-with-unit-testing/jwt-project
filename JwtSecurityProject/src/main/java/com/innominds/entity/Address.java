package com.innominds.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "address")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String line1;
	@Column
	private String line2;
	@Column
	private String pin;
	
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "userId")
	private User user;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) { 
		this.line2 = line2;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Address(int id, String line1, String line2, String pin, User user) {
		super();
		this.id = id;
		this.line1 = line1;
		this.line2 = line2;
		this.pin = pin;
		this.user = user;
	}
	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Address(int id, String line1, String line2, String pin) {
		super();
		this.id = id;
		this.line1 = line1;
		this.line2 = line2;
		this.pin = pin;
	}
	
	
	
	
	
}
