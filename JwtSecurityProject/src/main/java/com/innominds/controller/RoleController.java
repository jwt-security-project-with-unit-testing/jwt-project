package com.innominds.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.innominds.entity.Role;
import com.innominds.sevices.RoleService;

@RestController
public class RoleController {

	@Autowired
	private RoleService roleService;

	@PostMapping("/createNewRole")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<?> addNewRole(@RequestBody Role role) {
		return Response.success(roleService.createNewRole(role));
	}

	@GetMapping("/getAllRole")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<?> getAllRole() {
		return Response.success(roleService.findAll());
	}

	@PreAuthorize("hasRole('admin')")
	@DeleteMapping("/deleteRoleByName/{role}")
	public ResponseEntity<?> deleteRoleByName(@PathVariable("role") String role) {
		if (roleService.deleteRoleById(role))
			return Response.successMessage("record deleted");
		else {
			return Response.error("Record NOT deleted!!");
		}

	}
}
