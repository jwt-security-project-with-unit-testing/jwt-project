package com.innominds.controller;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.innominds.entity.User;
import com.innominds.sevices.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostConstruct // this API will be called automatically
	public String createCred() {
		userService.userInitMehtod();
		return "created data";
	}

	@PostMapping("/registerNewUser")
	public ResponseEntity<?> createNewUser(@RequestBody User user) {
		userService.createNewUser(user);
		return Response.successMessage("Record saved successfully");
	}

//	@GetMapping("/forAdmin") // for admin authorization testing
//	@PreAuthorize("hasRole('admin')")
//	public ResponseEntity<?> forAdmin() {
//		return Response.successMessage("url accessed by admin");
//	}

	@GetMapping("/getAllUsers")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<?> getAllUsers() {
		List<User> userList = userService.getAllUser();
		return Response.success(userList);
	}

//	@GetMapping("/forUser") // for user authorization testing
//	@PreAuthorize("hasRole('user')")
//	public ResponseEntity<?> forUser() {
//		return Response.successMessage("url accessed by user");
//	}

	@GetMapping("/getUserDetailsById/{id}")
	@PreAuthorize("hasAnyRole('admin','user')")
	public ResponseEntity<?> getUserDetailsById(@PathVariable("id") int id) {
		User user = userService.findUserById(id).get();
		return Response.success(user);
	}

	@DeleteMapping("/deleteUserById/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<?> deleteUserById(@PathVariable("id") int id) {
		if (userService.deleteUserByUserId(id))
			return Response.successMessage("record deleted");
		else {
			return Response.error("Record NOT deleted!!");
		}
	}

}
