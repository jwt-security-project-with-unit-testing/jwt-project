package com.innominds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.innominds.entity.JobHistory;
import com.innominds.entity.User;
import com.innominds.sevices.JobHistoryService;

@RestController
public class JobHistoryController {
	
	@Autowired
	private JobHistoryService jobHistoryService;
	
	@PostMapping("/addJobHistoryByUserId/{id}")
	public ResponseEntity<?> addJobHistory(@PathVariable("id") int id,@RequestBody JobHistory jobHistory) {
		return Response.success(jobHistoryService.addJobHistory(id,jobHistory));

	}

	@DeleteMapping("/deleteJobHistoryById/{id}")
	public ResponseEntity<?> deleteJobHistoryById(@PathVariable("id") int id) {
		if (jobHistoryService.deleteJobHistorById(id))
			return Response.successMessage("record deleted");
		else {
			return Response.error("Record NOT deleted!!");
		}
	}

	@GetMapping("/getJobHistoryById/{id}")
	public ResponseEntity<?> getJobHistoryById(@PathVariable("id") int id) {
		JobHistory jobHistory= jobHistoryService.getJobHistoryById(id);
		return Response.success(jobHistory);
	}
	
	
}
