package com.innominds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.innominds.dtoConverter.AddressDtoConverter;
import com.innominds.dtos.AddressDto;
import com.innominds.entity.Address;
import com.innominds.entity.User;
import com.innominds.sevices.AddressService;

@RestController
public class AddressController {

	@Autowired
	private AddressService addressService;
	@Autowired
	private AddressDtoConverter addressDtoConverter;

	@PostMapping("/addAddress")
	public ResponseEntity<?> addAddress(@RequestBody AddressDto aadressDto) {
		return Response.success(addressService.addAddress(addressDtoConverter.dtoConverter(aadressDto)));

	}

	@PostMapping("/updateAddressByUserId/{id}") // work on this part
	public ResponseEntity<?> updateAddress(@PathVariable("id") int id, @RequestBody AddressDto aadressDto) {
		return Response.success(addressService.updateAddress(id, addressDtoConverter.dtoConverter(aadressDto)));

	}

	@GetMapping("/getAddessDetailsById/{id}")
	public ResponseEntity<?> getUserDetailsById(@PathVariable("id") int id) {
		Address address = addressService.getaddressById(id);
		return Response.success(address);
	}

	@DeleteMapping("/deleteAddressByUserId/{UserName}")
	public ResponseEntity<?> deleteAddressByUserId(@PathVariable("UserName") int UserName) {
		if (addressService.deleteAddressByUserId(UserName))
			return Response.successMessage("record deleted");
		else {
			return Response.error("Record NOT deleted!!");
		}
	}

}
