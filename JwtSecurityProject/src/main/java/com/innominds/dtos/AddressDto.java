package com.innominds.dtos;

import org.springframework.stereotype.Component;

@Component
public class AddressDto {
	
	private int addressId;
private String line1;
private String line2;
private String pin;
private int userId;
@Override
public String toString() {
	return "AddressDto [addressId=" + addressId + ", line1=" + line1 + ", line2=" + line2 + ", pin=" + pin + ", userId="
			+ userId + "]";
}
public int getAddressId() {
	return addressId;
}
public void setAddressId(int addressId) {
	this.addressId = addressId;
}
public String getLine1() {
	return line1;
}
public void setLine1(String line1) {
	this.line1 = line1;
}
public String getLine2() {
	return line2;
}
public void setLine2(String line2) {
	this.line2 = line2;
}
public String getPin() {
	return pin;
}
public void setPin(String pin) {
	this.pin = pin;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public AddressDto(int addressId, String line1, String line2, String pin, int userId) {
	super();
	this.addressId = addressId;
	this.line1 = line1;
	this.line2 = line2;
	this.pin = pin;
	this.userId = userId;
}
public AddressDto() {
	super();
	// TODO Auto-generated constructor stub
}

}
