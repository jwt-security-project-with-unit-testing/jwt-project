package com.innominds.dtoConverter;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.innominds.dao.UserDao;
import com.innominds.dtos.AddressDto;
import com.innominds.entity.Address;
import com.innominds.entity.User;

@Component
public class AddressDtoConverter {

	@Autowired
	private UserDao userDao;

	public Address dtoConverter(AddressDto addressDto) {
		Address address = new Address();
		User user = userDao.findById(addressDto.getUserId()).get();
		address.setUser(user);

		address.setId(addressDto.getAddressId());

		address.setLine1(addressDto.getLine1());

		address.setLine1(addressDto.getLine1());

		address.setLine2(addressDto.getLine2());
		address.setPin(addressDto.getPin());

		return address;
	}
}
