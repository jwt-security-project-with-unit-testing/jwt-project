package com.innominds.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.innominds.entity.JobHistory;

@Repository
public interface JobHistoryDao extends JpaRepository<JobHistory, Integer> {

}
