package com.innominds.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.innominds.entity.Role;
import com.innominds.entity.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
	public User findByFirstName(String firstName);
	
public	List<User> findByRole(String role);

public User findByUserName(String userName);

public void deleteById(String string);





}
