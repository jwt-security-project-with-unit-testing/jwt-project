package com.innominds.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.innominds.dtos.AddressDto;
import com.innominds.entity.Address;

@Repository
public interface AddressDao extends JpaRepository<Address, Integer> {

	public void deleteById(String userName);


	public Address save(AddressDto address);


	public Address findById(int id);


	public Address findByUserId(int id); //done update


	//@Query(value = "select * from product order by productAddedDate desc", nativeQuery = true)
	public void deleteByUserId(int userId);

	
	//@Query(value = "select * from product order by productAddedDate desc", nativeQuery = true)
//	public void findByUserId(int id);

	
	

	

}
