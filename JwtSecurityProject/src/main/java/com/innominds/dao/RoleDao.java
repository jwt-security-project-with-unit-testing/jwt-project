package com.innominds.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.innominds.entity.Role;

@Repository
public interface RoleDao  extends JpaRepository<Role, String> {

	
}
