package com.innominds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.innominds.sevices.UserService;

@SpringBootApplication
public class JwtSecurityProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSecurityProjectApplication.class, args);
		System.out.println("program started");
		
	
	}

}
