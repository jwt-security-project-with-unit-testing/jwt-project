package com.innominds.sevices;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innominds.dao.AddressDao;
import com.innominds.dtos.AddressDto;
import com.innominds.entity.Address;

@Service
@Transactional
public class AddressService {

	@Autowired
	private AddressDao addressDao;

	public Address addAddress(Address address) {
		return addressDao.save(address);
	}

	public Address getaddressById(int id) {
		return addressDao.findById(id);
	}
	
	public boolean deleteAddressByUserId(int id) { // delete
		if (addressDao.existsById(id)) {
			addressDao.deleteByUserId(id);
			return true;
		} else {
			return false;
		}
	}

	public Address updateAddress(int id, Address address) {
//		Address address1 = addressDao.findByUserId(id);
		return addressDao.save(address);
	}

	public List<Address> getAllAddress() {
		return addressDao.findAll();
	}

//	public void getAddressByUsername(String userName) {
//		addressDao.(userName);
//	}
}
