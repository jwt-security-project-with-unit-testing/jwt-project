package com.innominds.sevices;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.memory.UserAttribute;
import org.springframework.stereotype.Service;

import com.innominds.dao.JobHistoryDao;
import com.innominds.dao.UserDao;
import com.innominds.entity.JobHistory;
import com.innominds.entity.User;

@Service
public class JobHistoryService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private JobHistoryDao jobHistoryDao;

	public JobHistory addJobHistory(int id, JobHistory jobHistory) {
		User user = new User();
		List<JobHistory> jobHistoryList = new ArrayList<JobHistory>();
		user = userDao.findById(id).get();
		user.setJobHistory(jobHistoryList);
		jobHistory.setUser(user);
		return jobHistoryDao.save(jobHistory);
	}

	public boolean deleteJobHistorById(int id) {
		if (jobHistoryDao.existsById(id)) {
			jobHistoryDao.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

//	public List<JobHistory> getAllJob 

	public JobHistory getJobHistoryById(int id) {
		return jobHistoryDao.findById(id).get();
	}
}
