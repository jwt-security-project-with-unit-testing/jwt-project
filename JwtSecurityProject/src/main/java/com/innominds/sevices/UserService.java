package com.innominds.sevices;

import java.security.spec.EncodedKeySpec;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.innominds.dao.RoleDao;
import com.innominds.dao.UserDao;
import com.innominds.entity.Address;
import com.innominds.entity.Role;
import com.innominds.entity.User;

@Transactional
@Service
public class UserService {
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserDao userDao;
	@Autowired
	private RoleDao roleDao;

	public void userInitMehtod() {
		Role adminRole = new Role();
		adminRole.setRoleDescription("admin role");
		adminRole.setRoleName("admin");
		roleDao.save(adminRole);

		Role userRole = new Role();
		userRole.setRoleName("user");
		userRole.setRoleDescription("user role");
		roleDao.save(userRole);

		User admin = new User();
		admin.setId(1);
		admin.setFirstName("amit");
		admin.setLastName("anwade");
		admin.setUserName("amit@gmail.com");
		admin.setPassword(getEncodedPassword("password"));
		Set<Role> role2 = new HashSet<>();
		role2.add(adminRole);
		admin.setRole(role2);
		userDao.save(admin);

		User user = new User();
		user.setId(2);
		user.setFirstName("amol");
		user.setLastName("anwade");
		user.setUserName("amol@gmail.com");
		user.setPassword(getEncodedPassword("password"));
		Set<Role> role3 = new HashSet<>();
		role3.add(userRole);
		user.setRole(role3);
		userDao.save(user);
		;
	}

	public String getEncodedPassword(String password) {
		return passwordEncoder.encode(password);
	}

	public User createNewUser(User user) {
		Role role = roleDao.findById("user").get(); // getting already saved role info from DB
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user.setRole(roles);
		user.setPassword(getEncodedPassword(user.getPassword()));
		return userDao.save(user);
	}

	public List<User> getAllUser() { // get all user
		System.out.println(userDao.findAll().toString());
		return userDao.findAll();
	}

	public User findUserByUserName(String userName) { // find User By User Name
		User user = userDao.findByUserName(userName);
		return userDao.findByUserName(userName);
	}

	public User findByName(String name) { // find By Name
		return userDao.findByFirstName(name);
	}

	public List<User> findByUserRole(String role) { // find By User Role
		List<User> user = userDao.findByRole(role);
		return userDao.findByRole(role);
	}

	public boolean deleteUserByUserId(int id) {	//delete User By User Name
		if(userDao.existsById(id)) {
			userDao.deleteById(id);
			return true;
		}else {
		return false;}
	}

	public Optional<User> findUserById(int id) {
		return userDao.findById(id);
	}
	
	public User updateUser(int id,User user) {
//		User user1 = userDao.findById(id).get();
//		
//		if (user1.getFirstName() != null) {
//			user1.setFirstName(user.getFirstName());
//		}
//		if (user1.getLastName() != null) {
//			user1.setLastName(user.getLastName());
//		}
//
//		if (user1.getUserName() != null) {
//			user1.setUserName(user.getUserName());
//		}
//
//		if (user1.getPassword() != null) {
//			user.setPassword(getEncodedPassword(user.getPassword()));
//		}
		return userDao.save(user);
	}
}
