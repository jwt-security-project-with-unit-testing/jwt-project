package com.innominds.sevices;

import java.util.List;

import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innominds.dao.RoleDao;
import com.innominds.entity.Role;

@Service
public class RoleService {
	@Autowired
	private RoleDao roleDao;

	public Role createNewRole(Role role) {
		return roleDao.save(role);
	}

	public List<Role> findAll() {
		return roleDao.findAll();
	}

	public boolean deleteRoleById(String role) {

		if (roleDao.existsById(role)) {
			roleDao.deleteById(role);
			return true;
		} else {
			return false;
		}
	}
}
