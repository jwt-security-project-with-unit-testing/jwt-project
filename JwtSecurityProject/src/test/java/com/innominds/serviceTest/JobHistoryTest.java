package com.innominds.serviceTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.innominds.dao.AddressDao;
import com.innominds.dao.JobHistoryDao;
import com.innominds.entity.Address;
import com.innominds.entity.JobHistory;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.AddressService;
import com.innominds.sevices.JobHistoryService;


@SpringBootTest
@RunWith(SpringRunner.class)
class JobHistoryTest {

	
	@Autowired
	private JobHistoryService jobHistoryService;
	@MockBean
	private JobHistoryDao jobHistoryDao;
	
	
	
	@Test
	public void addJobHistoryTest() {
	JobHistory jobHistory = new JobHistory(1,"SPDE","java development");
		when(jobHistoryDao.save(jobHistory)).thenReturn(jobHistory);
		assertEquals("SPDE", jobHistoryService.addJobHistory(1,jobHistory).getJobName());
	}
	
	
	
	
	
	@Test
	public void getJobHistoryById() {
		Optional<JobHistory> jobHistory = Optional.of(new JobHistory(1,"SPDE","java development"));
		when(jobHistoryDao.findById(1)).thenReturn(jobHistory);
	assertEquals("SPDE", jobHistoryService.getJobHistoryById(1).getJobName());
	}
	
	@Test
	public void deleteJobHistorTrueById() {
		JobHistory jobHistory = new JobHistory(1,"SPDE","java development");
		jobHistoryService.addJobHistory(1, jobHistory);
		Mockito.when(jobHistoryDao.existsById(1)).thenReturn(true);
		Mockito.doNothing().when(jobHistoryDao).deleteById(1); 
	assertEquals(true, jobHistoryService.deleteJobHistorById(1));
	}
	
	@Test
	public void deleteJobHistorFalseById() {
		JobHistory jobHistory = new JobHistory(1,"SPDE","java development");
		jobHistoryService.addJobHistory(1, jobHistory);
		Mockito.when(jobHistoryDao.existsById(1)).thenReturn(false);
		Mockito.doNothing().when(jobHistoryDao).deleteById(1); 
	assertEquals(false, jobHistoryService.deleteJobHistorById(1));
	}	
	
	
}
