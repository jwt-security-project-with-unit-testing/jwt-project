package com.innominds.serviceTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.innominds.dao.AddressDao;
import com.innominds.entity.Address;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.AddressService;

@SpringBootTest
@RunWith(SpringRunner.class)
class AddressTest {

	@Autowired
	private AddressService addressService;
	@MockBean
	private AddressDao addressDao;

	@Test
	public void getAllAddressTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		when(addressDao.findAll()).thenReturn(
				Stream.of(new Address(1, "chinchwad", "pune", "431103", user)).collect(Collectors.toList()));
		assertEquals(1, addressService.getAllAddress().size());
	}

	@Test
	public void getAddressByIdTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		when(addressDao.findById(1)).thenReturn(address);
		assertEquals(address, addressService.getaddressById(1));
	}

	
	@Test
	public void addAddressTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		when(addressDao.save(address)).thenReturn(address);

		assertEquals("431103", addressService.addAddress(address).getPin());
	}

	@Test
	public void deleteAddressByUserId() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		when(addressDao.save(address)).thenReturn(address);
		addressDao.save(address);
		Mockito.doNothing().when(addressDao).deleteById("aj@gmail.com");
		assertEquals(0, addressService.getAllAddress().size());
	}

	@Test
	public void updateAddressTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		when(addressDao.save(address)).thenReturn(address);

		assertEquals(address, addressService.updateAddress(1, address));
	}

	@Test
	public void deleteByIdTrueTest() {
		Address address = new Address(1, "sri vasavi pg", "gowlidoddy, hyderabad", "500032");
		when(addressDao.findById(1)).thenReturn(address);
		when(addressDao.existsById(1)).thenReturn(true);
		Mockito.doNothing().when(addressDao).deleteById(1);
		assertTrue(addressService.deleteAddressByUserId(1));
	}
	@Test
	public void deleteByIdFalseTest() {
		Address address = new Address(1, "sri vasavi pg", "gowlidoddy, hyderabad", "500032");
		when(addressDao.existsById(1)).thenReturn(false);
		Mockito.doNothing().when(addressDao).deleteById(1);
		assertFalse(addressService.deleteAddressByUserId(1));
	}
}
