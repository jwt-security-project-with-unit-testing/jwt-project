package com.innominds.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.innominds.dao.RoleDao;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.RoleService;

@SpringBootTest
@RunWith(SpringRunner.class)
class RoleTest {
	@Autowired
	private RoleService roleService;
	@MockBean
	private RoleDao roleDao;

	@Test
	public void createNewRoleTest() {
		Role role = new Role("user", "user Role Description");
		when(roleDao.save(role)).thenReturn(role);
		assertThat(roleService.createNewRole(role)).isEqualTo(role);
	}

	@Test
	public void getAllRolesTest() {
		List<Role> roleSet = new ArrayList<Role>();
		Role adminRole = new Role("user", "user Role Description");
		Role userRole = new Role("admin", "admin Role Description");
		roleSet.add(adminRole);
		roleSet.add(userRole);
		when(roleDao.findAll()).thenReturn(roleSet);
		assertEquals(roleSet, roleService.findAll());
	}

	@Test
	public void deleteByIdTrueTest() {
		List<Role> roleSet = new ArrayList<Role>();
		Role adminRole = new Role("user", "user Role Description");
		Role userRole = new Role("admin", "admin Role Description");
		roleSet.add(adminRole);
		roleSet.add(userRole);

		when(roleDao.existsById("user")).thenReturn(true);
		Mockito.doNothing().when(roleDao).deleteById("user");
		assertTrue(roleService.deleteRoleById("user"));
	}
	
	@Test
	public void deleteByIdFalseTest() {
		List<Role> roleSet = new ArrayList<Role>();
		Role adminRole = new Role("user", "user Role Description");
		Role userRole = new Role("admin", "admin Role Description");
		roleSet.add(adminRole);
		roleSet.add(userRole);

		when(roleDao.existsById("user")).thenReturn(false);
		Mockito.doNothing().when(roleDao).deleteById("user");
		assertFalse(roleService.deleteRoleById("user"));
	}
}
