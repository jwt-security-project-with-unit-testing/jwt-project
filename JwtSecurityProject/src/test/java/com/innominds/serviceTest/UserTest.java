package com.innominds.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.innominds.dao.UserDao;
import com.innominds.entity.Address;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.UserService;

@SpringBootTest
@RunWith(SpringRunner.class)
class UserTest {

	@Autowired
	private UserService userService;
	@MockBean
	private UserDao userDao;

	@Test
	public void getAllUserTest() {
		Set<Role> roleSet =new  HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		when(userDao.findAll()).thenReturn(Stream.of(new User(1,"aj@gmail.com","ajinkya","anwade","password",roleSet),new User(1,"ami@gmail.com","ami","anwade","password",roleSet)).collect(Collectors.toList())); 
	
	assertEquals(2, userService.getAllUser().size());
	}

	@Test
	public void findUserByUserNameTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
User user = (new  User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet));
		when(userDao.findByUserName("aj@gmail.com")).thenReturn(user);

		assertEquals("aj@gmail.com", userService.findUserByUserName("aj@gmail.com").getUserName());
	}

	
	
	@Test
	public void findByFirstNameTest() {
		Set<Role> roleSet =new  HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = (new  User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet));
		when(userDao.findByFirstName("ajinkya")).thenReturn(user);
		
	assertEquals("ajinkya", userService.findByName("ajinkya").getFirstName());
	}
	
	@Test
	public void findByIdTest() {
		Set<Role> roleSet =new  HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		 Optional<User> user = Optional.of(new  User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet));
		when(userDao.findById(1)).thenReturn(user);
		
		assertThat(userService.findUserById(1)).isEqualTo(user);
//	assertEquals("ajinkya", userService.findByName("ajinkya").getFirstName());
	}
	
	@Test
	public void findByUserRoleTest() {
		Set<Role> roleSet =new  HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		when(userDao.findByRole("user")).thenReturn(Stream.of(new User(1,"aj@gmail.com","ajinkya","anwade","password",roleSet),new User(1,"ami@gmail.com","ami","anwade","password",roleSet)).collect(Collectors.toList())); 
		
	assertEquals(2, userService.findByUserRole("user").size());
	}
	
	@Test
	public void deleteByIdTrueTest() {
		Set<Role> roleSet =new  HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
	User user = (new  User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet));
	
when(userDao.existsById(1)).thenReturn(true);
Mockito.doNothing().when(userDao).deleteById("aj@gmail.com");
	assertTrue(userService.deleteUserByUserId(1));
	}
	@Test
	public void deleteByIdFalseTest() {
		Set<Role> roleSet =new  HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
	User user = (new  User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet));
	
when(userDao.existsById(1)).thenReturn(false);
Mockito.doNothing().when(userDao).deleteById("aj@gmail.com");
	assertFalse(userService.deleteUserByUserId(1));
	}
	
	@Test
	public void createNewUserTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		when(userDao.save(user)).thenReturn(user);
		assertThat(userService.createNewUser(user)).isEqualTo(user);
	}
	
	
	@Test
	public void updateUserTest() {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		when(userDao.save(user)).thenReturn(user);
		assertEquals(user, userService.updateUser(1, user));
	}
	
	
//	public static User returnUser() {
//		Set<Role> roleSet = new HashSet<>();
//		Role role = new Role();
//		role.setRoleName("user");
//		role.setRoleDescription("user role description");
//		roleSet.add(role);
//		User user = new User(1,"aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
//		return user;
//	}
}
