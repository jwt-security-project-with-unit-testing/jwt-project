package com.innominds.controllerTest;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.configuration.JwtAuthenticationEntryPoint;
import com.innominds.configuration.JwtRequestFilter;
import com.innominds.controller.AddressController;
import com.innominds.controller.RoleController;
import com.innominds.dao.AddressDao;
import com.innominds.dao.RoleDao;
import com.innominds.dao.UserDao;
import com.innominds.dtoConverter.AddressDtoConverter;
import com.innominds.dtos.AddressDto;
import com.innominds.entity.Address;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.AddressService;
import com.innominds.sevices.JwtService;
import com.innominds.sevices.RoleService;
import com.innominds.sevices.UserService;
import com.innominds.utils.JwtUtil;



@WebMvcTest(value = AddressController.class)
@AutoConfigureMockMvc(addFilters = false)
class AddressControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AddressService addressService;

	@MockBean
	private AddressDtoConverter addressDtoConverter;

	@MockBean
	private AddressDao addressDao;

	@MockBean
	private UserService userService;

	@MockBean
	private UserDao userDao;

	@MockBean
	private JwtUtil jwtUtil;
	@MockBean
	private JwtService jwtService;

	@MockBean
	private JwtRequestFilter jwtRequestFilter;

	@MockBean
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;



	@WithMockUser(roles = "admin")
	@Test
	public void addAddressTest() throws Exception {

		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		
		ObjectMapper mapper = new ObjectMapper();
		Mockito.when(addressService.addAddress(Mockito.any())).thenReturn(address);
		Mockito.when(addressDao.save(address)).thenReturn(address);
		String uri = "/addAddress";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(address));
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
	} 
	
	
	@Test
	public void updateAddressByUserIdTest() throws Exception {

		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		Address address1 = addressDtoConverter.dtoConverter(new AddressDto(1, "chinchwad", "pune", "431103", 1));
		
		
		
		ObjectMapper mapper = new ObjectMapper();
		Mockito.when(addressService.updateAddress(1,address)).thenReturn(address);
		Mockito.when(addressDao.save(address)).thenReturn(address);
		String uri = "/updateAddressByUserId/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(address));
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
	} 
	
	@Test
	public void deleteAddressByUserIdTrueTest() throws Exception {

		Mockito.when(addressService.deleteAddressByUserId(1)).thenReturn(true);
		String uri = "/deleteAddressByUserId/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}
	
	@Test
	public void deleteAddressByUserIdFalseTest() throws Exception {

		Mockito.when(addressService.deleteAddressByUserId(1)).thenReturn(false);
		String uri = "/deleteAddressByUserId/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}	
	
	@Test
	public void getAddessDetailsById() throws Exception {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		Address address = new Address(1, "chinchwad", "pune", "431103", user);
		
		Mockito.when(addressService.getaddressById(1)).thenReturn(address);
		String uri = "/getAddessDetailsById/1";
		MvcResult mvcResult = mockMvc.perform(get(uri)).andExpect(status().isOk()).andReturn();
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println("--------------------------------" + outputJson);
	}
}
