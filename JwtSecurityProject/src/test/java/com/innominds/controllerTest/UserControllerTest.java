package com.innominds.controllerTest;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.configuration.JwtAuthenticationEntryPoint;
import com.innominds.configuration.JwtRequestFilter;
import com.innominds.controller.UserController;
import com.innominds.dao.UserDao;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.JwtService;
import com.innominds.sevices.UserService;
import com.innominds.utils.JwtUtil;

//@SpringBootTest
//@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class)
@AutoConfigureMockMvc(addFilters = false)
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;

	@MockBean
	private UserDao userDao;
	@MockBean
	private com.innominds.dao.RoleDao RoleDao;
	@MockBean
	private com.innominds.sevices.RoleService RoleService;
	@MockBean
	private JwtUtil jwtUtil;
	@MockBean
	private JwtService jwtService;

	@MockBean
	private JwtRequestFilter jwtRequestFilter;

	@MockBean
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	ObjectMapper om = new ObjectMapper();

	@Autowired
	private WebApplicationContext context;

	@Test
	public void createNewUserTest() throws Exception {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add(role);
		User user = new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		ObjectMapper mapper = new ObjectMapper();
		Mockito.when(userService.createNewUser(Mockito.any())).thenReturn(user);
		Mockito.when(userDao.save(user)).thenReturn(user);
		String uri = "/registerNewUser";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(user));
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
	}

	@WithMockUser(roles = "admin")
	@Test
	public void getAllUsersTest() throws Exception {
		List<User> userList = new ArrayList<User>();
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("admin");
		role.setRoleDescription("role description");
		roleSet.add(role);
		User user1 = new User(2, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet);
		User user2 = new User(4, "ajk@gmail.com", "ajinkya", "anwade", "password", roleSet);
		userList.add(user1);
		userList.add(user2);
		Mockito.when(userService.getAllUser()).thenReturn(userList);
		String uri = "/getAllUsers";
		MvcResult mvcResult = mockMvc.perform(get(uri)).andExpect(status().isOk()).andReturn();
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println("--------------------------------" + outputJson);
	}

	@WithMockUser(roles = "admin")
	@Test
	public void getUserDetailsByIdTest() throws Exception {
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("admin");
		role.setRoleDescription(
				"mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn(); role description");
		roleSet.add(role);
		Optional<User> user = Optional.of(new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet));

		Mockito.when(userService.findUserById(1)).thenReturn(user);
		Mockito.when(userDao.findById(1)).thenReturn(user);
		String uri = "/getUserDetailsById/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}

	@WithMockUser(roles = "admin")
	@Test
	public void deleteUserByIdTrueTest() throws Exception {
//		Set<Role> roleSet = new HashSet<>();
//		Role role = new Role();
//		role.setRoleName("admin");
//		role.setRoleDescription(
//				"mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn(); role description");
//		roleSet.add(role);
//		Optional<User> user = Optional.of(new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet));

		Mockito.when(userService.deleteUserByUserId(1)).thenReturn(true);
//		Mockito.when(userDao.existsById(1)).thenReturn(true);
		String uri = "/deleteUserById/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}

	@WithMockUser(roles = "admin")
	@Test
	public void deleteUserByIdFalseTest() throws Exception {
//		Set<Role> roleSet = new HashSet<>();
//		Role role = new Role();
//		role.setRoleName("admin");
//		role.setRoleDescription(
//				"mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn(); role description");
//		roleSet.add(role);
//		Optional<User> user = Optional.of(new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet));

		Mockito.when(userService.deleteUserByUserId(1)).thenReturn(false);
//		Mockito.when(userDao.existsById(1)).thenReturn(true);
		String uri = "/deleteUserById/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}
}
