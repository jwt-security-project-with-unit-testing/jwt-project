package com.innominds.controllerTest;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.configuration.JwtAuthenticationEntryPoint;
import com.innominds.configuration.JwtRequestFilter;
import com.innominds.controller.RoleController;
import com.innominds.controller.UserController;
import com.innominds.dao.RoleDao;
import com.innominds.dao.UserDao;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.JwtService;
import com.innominds.sevices.RoleService;
import com.innominds.sevices.UserService;
import com.innominds.utils.JwtUtil;

@WebMvcTest(value = RoleController.class)
@AutoConfigureMockMvc(addFilters = false)
class RoleControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RoleService roleService;

	@MockBean
	private RoleDao roleDao;

	@MockBean
	private UserService userService;

	@MockBean
	private UserDao userDao;

	@MockBean
	private JwtUtil jwtUtil;
	@MockBean
	private JwtService jwtService;

	@MockBean
	private JwtRequestFilter jwtRequestFilter;

	@MockBean
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@WithMockUser(roles = "admin")
	@Test
	public void addNewRoleTesta() throws Exception {

		Role role = new Role( "admin", "user Role Description");

		ObjectMapper mapper = new ObjectMapper();
		Mockito.when(roleService.createNewRole(Mockito.any())).thenReturn(role);
		Mockito.when(roleDao.save(role)).thenReturn(role);
		String uri = "/createNewRole";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(role));
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
	}

	@WithMockUser(roles = "admin")
	@Test
	public void getAllRoleTest() throws Exception {
		List<Role> roleSet = new ArrayList<Role>();
		Role role1 = new Role("admin","admin role");
		Role role2 = new Role("user","user role");
		roleSet.add(role1);
		roleSet.add(role2);
		Mockito.when(roleService.findAll()).thenReturn(roleSet);
		String uri = "/getAllRole";
		MvcResult mvcResult = mockMvc.perform(get(uri)).andExpect(status().isOk()).andReturn();
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println("--------------------------------" + outputJson);
	}

	@WithMockUser(roles = "admin")
	@Test
	public void deleteRoleByNameTrueTest() throws Exception {

		Mockito.when(roleService.deleteRoleById("user")).thenReturn(true);
		String uri = "/deleteRoleByName/user";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}
	
	@WithMockUser(roles = "admin")
	@Test
	public void deleteRoleByNamefalseTest() throws Exception {

		Mockito.when(roleService.deleteRoleById("user")).thenReturn(false);
		String uri = "/deleteRoleByName/user";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}	
	
}
