package com.innominds.controllerTest;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.configuration.JwtAuthenticationEntryPoint;
import com.innominds.configuration.JwtRequestFilter;
import com.innominds.controller.AddressController;
import com.innominds.controller.JobHistoryController;
import com.innominds.dao.JobHistoryDao;
import com.innominds.dao.RoleDao;
import com.innominds.dao.UserDao;
import com.innominds.entity.Address;
import com.innominds.entity.JobHistory;
import com.innominds.entity.Role;
import com.innominds.entity.User;
import com.innominds.sevices.JobHistoryService;
import com.innominds.sevices.JwtService;
import com.innominds.sevices.RoleService;
import com.innominds.sevices.UserService;
import com.innominds.utils.JwtUtil;

@WebMvcTest(value = JobHistoryController.class)
@AutoConfigureMockMvc(addFilters = false)
class JobHistoryControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private JobHistoryService jobHistoryService;

	@MockBean
	private JobHistoryDao jobHistoryDao;

	@MockBean
	private UserService userService;

	@MockBean
	private UserDao userDao;
	@MockBean
	private RoleService roleService;

	@MockBean
	private RoleDao roleDao;

	@MockBean
	private JwtUtil jwtUtil;
	@MockBean
	private JwtService jwtService;

	@MockBean
	private JwtRequestFilter jwtRequestFilter;

	@MockBean
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;


	
	@Test
	public void addJobHistoryByUserIdTest() throws Exception {

		JobHistory jobHistory = new JobHistory(1, "SPDE", "java developer");
		Set<Role> roleSet = new HashSet<>();
		Role role = new Role();
		role.setRoleName("user");
		role.setRoleDescription("user role description");
		roleSet.add (role);
		Optional<User> user = Optional.of(new User(1, "aj@gmail.com", "ajinkya", "anwade", "password", roleSet));

		ObjectMapper mapper = new ObjectMapper();

		Mockito.when(jobHistoryService.addJobHistory(1, jobHistory)).thenReturn(jobHistory);
		Mockito.when(userDao.findById(1)).thenReturn(user);

		Mockito.when(jobHistoryDao.save(jobHistory)).thenReturn(jobHistory);
		String uri = "/addJobHistoryByUserId/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(jobHistory));
		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
		assertNotNull(mvcResult.getResponse().getContentAsString());
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println(outputJson);
	}
	
	
	@Test
	public void deleteJobHistoryByIdfalseTest() throws Exception {

		Mockito.when(jobHistoryService.deleteJobHistorById(1)).thenReturn(false);
		String uri = "/deleteJobHistoryById/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}	
	@Test
	public void deleteJobHistoryByIdTrueTest() throws Exception {

		Mockito.when(jobHistoryService.deleteJobHistorById(1)).thenReturn(true);
		String uri = "/deleteJobHistoryById/1";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE);

		MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();

		String output = mvcResult.getResponse().getContentAsString();
		System.out.println("---------------------------------------" + output);
	}	
	
	

	@Test
	public void getJobHistoryByIdTest() throws Exception {
		JobHistory jobHistory = new JobHistory(1, "SPDE", "java developer");
		Mockito.when(jobHistoryService.getJobHistoryById(1)).thenReturn(jobHistory);
		String uri = "/getJobHistoryById/1";
		MvcResult mvcResult = mockMvc.perform(get(uri)).andExpect(status().isOk()).andReturn();
		String outputJson = mvcResult.getResponse().getContentAsString();
		System.out.println("--------------------------------" + outputJson);
	}
	
}
