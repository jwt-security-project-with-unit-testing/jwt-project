//package com.innominds.controllerTest;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.Set;
//import java.util.UUID;
//
//import org.junit.Before;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.nice.saas.wfo.cxpm.api.service.CoachingSessionService;
//import com.nice.saas.wfo.cxpm.api.service.coaching.*;
//
//public class CoachingSessionControllerTest {
//
//	private MockMvc mockMvc;
//	@InjectMocks
//	private CoachingSessionController coachingSessionController;
//	@Mock
//	private CoachingSessionService coachingSessionService;
//
//	private final ObjectMapper mapper = new ObjectMapper();
//
//	@Before
//	public void setUp() {
//		MockitoAnnotations.initMocks(this);
//		mockMvc = MockMvcBuilders.standaloneSetup(coachingSessionController).build();
//	}
//
//	@Test
//	public void shouldSaveNewCoachingSession() throws Exception {
//		CoachingSessionRequest request = new CoachingSessionRequest();
//		request.setSessionName("test");
//		request.setSessionTypeId(1);
//		request.setSessionState("Draft");
//		request.setObjective("Test Objective");
//		request.setFocusAreaId(1);
//		request.setBehaviorId(3);
//		request.setParticipants(Arrays.asList(UUID.randomUUID()));
//		Mockito.when(coachingSessionService.saveCoachingSession(Mockito.any()))
//				.thenReturn(new CoachingSessionResponse());
//		mockMvc.perform(post(Constants.CXPM_COACHING_MANAGER_BASE_URL + Constants.V1_COACHING_SESSION)
//				.content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isCreated());
//	}
//
//	@Test
//	public void shouldSearchCoachingSession() throws Exception {
//		CoachingSearchCriteria coachingSearchCriteria = new CoachingSearchCriteria();
//		Filter filter = new Filter(Field.name, Operation.LIKE, new String[] { "Test" });
//		coachingSearchCriteria.setFilters(Lists.newArrayList(filter));
//		Mockito.when(coachingSessionService.searchCoachingSessions(coachingSearchCriteria))
//				.thenReturn(new CoachingSearchResponse());
//		mockMvc.perform(post(Constants.CXPM_COACHING_MANAGER_BASE_URL + Constants.V1_COACHING_SESSION)
//				.content(mapper.writeValueAsString(coachingSearchCriteria)).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isCreated());
//	}
//
//	@Test
//	public void shouldSendReminderToCoachingSession() throws Exception {
//		SendReminderRequest request = new SendReminderRequest();
//		Set<UUID> coachingIds = new HashSet<>();
//		coachingIds.add(UUID.randomUUID());
//		request.setCoachingIds(coachingIds);
//		Mockito.when(coachingSessionService.sendReminder(request)).thenReturn(new SendReminderResponse());
//		mockMvc.perform(post(Constants.CXPM_COACHING_MANAGER_BASE_URL + Constants.V1_COACHING_SESSION_NOTIFY)
//				.content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//	}
//
//	@Test
//	public void shouldDeleteCoachingSession() throws Exception {
//		DeleteCoachingSessionRequest request = new DeleteCoachingSessionRequest();
//		Set<UUID> coachingIds = new HashSet<>();
//		coachingIds.add(UUID.randomUUID());
//		request.setCoachingIds(coachingIds);
//	}
//}